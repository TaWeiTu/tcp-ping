#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>

#include <array>
#include <iostream>
#include <mutex>
#include <thread>

constexpr int kMaxConnections = 16;
constexpr int kParallelConnections = 8;

int main(int argc, const char **argv) {
  if (argc < 2) {
    std::cout << "[Usage] ./server port" << std::endl;
    exit(1);
  }
  int port = atoi(argv[1]), svr_fd = 0;
  if ((svr_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    std::cout << "failed at socket()" << std::endl;
    exit(1);
  }
  sockaddr_in server;
  server.sin_family = AF_INET;
  server.sin_port = htons(port);
  server.sin_addr.s_addr = htonl(INADDR_ANY);
  if (bind(svr_fd, reinterpret_cast<sockaddr *>(&server), sizeof(server)) < 0) {
    std::cout << "failed at bind()" << std::endl;
    exit(1);
  }
  if (listen(svr_fd, kMaxConnections) < 0) {
    std::cout << "failed at listen()" << std::endl;
    exit(1);
  }

  std::mutex mtx;
  auto AcceptRequest = [svr_fd, &mtx]() {
    while (true) {
      sockaddr_in client;
      int client_size = sizeof(client);
      int cli_fd = accept(svr_fd, reinterpret_cast<sockaddr *>(&client),
                          reinterpret_cast<socklen_t *>(&client_size));
      if (cli_fd < 0) {
        mtx.lock();
        std::cout << "failed at accept()" << std::endl;
        mtx.unlock();
        return;
      }
      mtx.lock();
      std::cout << "recv from " << inet_ntoa(client.sin_addr) << ":"
                << ntohs(client.sin_port) << std::endl;
      mtx.unlock();
    }
  };

  std::array<std::thread, kParallelConnections> pool{};
  for (int i = 0; i < kParallelConnections; ++i) {
    pool[i] = std::thread(AcceptRequest);
  }
  for (int i = 0; i < kParallelConnections; ++i) pool[i].join();
  return 0;
}
