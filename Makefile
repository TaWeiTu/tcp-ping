CXXFLAGS = -O3 -std=c++14 -pthread

all: server client

server: server.cc
	$(CXX) $(CXXFLAGS) server.cc -o server

client: client.cc
	$(CXX) $(CXXFLAGS) client.cc -o client

.PHONY: clean
clean: 
	$(RM) ./server ./client
