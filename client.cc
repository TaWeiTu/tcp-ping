#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <signal.h>
#include <sys/socket.h>
#include <unistd.h>

#include <algorithm>
#include <array>
#include <chrono>
#include <cmath>
#include <cstring>
#include <future>
#include <iomanip>
#include <iostream>
#include <mutex>
#include <string>
#include <thread>
#include <utility>
#include <vector>

constexpr size_t kDefaultNumPing = 0;
constexpr uint32_t kDefaultTimeout = 1000;
constexpr uint32_t kPingInterval = 500;

struct Argument {
  size_t num_ping;   // the number of pings, if num_ping = 0, then the client
                     // won't stop pinging.
  uint32_t timeout;  // the time limit of connection (in milliseconds).
  uint32_t ping_interval;  // the interval between two consecutive pings (in
                           // milliseconds).
  std::vector<std::pair<in_addr_t, uint16_t>> ip_ports;

  Argument()
      : num_ping(kDefaultNumPing),
        timeout(kDefaultTimeout),
        ping_interval(kPingInterval) {}

  template <class... Param>
  void push(Param &&... param) {
    ip_ports.emplace_back(std::forward<Param>(param)...);
  }
};

struct Statistic {
  double min_rtt, max_rtt, sum_rtt, sum_square;
  int success, attempt;

  Statistic()
      : min_rtt(1e9),
        max_rtt(0),
        sum_rtt(0),
        sum_square(0),
        success(0),
        attempt(0) {}

  void Failed() { attempt += 1; }

  void Success(double rtt) {
    attempt += 1;
    success += 1;
    min_rtt = std::min(min_rtt, rtt);
    max_rtt = std::max(max_rtt, rtt);
    sum_rtt += rtt;
    sum_square += rtt * rtt;
  }

  friend std::ostream &operator<<(std::ostream &os, const Statistic stat) {
    double loss = 100.0 * (stat.attempt - stat.success) / stat.attempt;
    os << stat.attempt << " packets transmitted, " << stat.success
       << " packets received, " << std::fixed << std::setprecision(1) << loss
       << "\% packet loss\n";
    double min = stat.success == 0 ? 0.0 : stat.min_rtt;
    double max = stat.success == 0 ? 0.0 : stat.max_rtt;
    double avg = stat.success == 0 ? 0.0 : stat.sum_rtt / stat.success;
    double stddev = stat.success == 0
                        ? 0.0
                        : sqrt((stat.sum_square - 2 * stat.sum_rtt * avg +
                                stat.success * avg * avg) /
                               stat.success);
    os << "round-trip min/avg/max/stddev = " << std::fixed
       << std::setprecision(3) << min << "/" << avg << "/" << max << "/"
       << stddev << " ms";
    return os;
  }
};

namespace {

void Usage() {
  std::cout << "[Usage] ./client [-n number] [-t timeout] host_1:port_1 "
               "host_2:port_2 ..."
            << std::endl;
  exit(1);
}

in_addr_t ParseIP(const std::string &s) {
  std::vector<std::string> cut;
  size_t prv = 0;
  for (size_t i = 0; i < s.size(); ++i) {
    if (s[i] == '.') {
      cut.emplace_back(s.substr(prv, i - prv));
      prv = i + 1;
    }
  }
  cut.emplace_back(s.substr(prv));
  if (cut.empty()) throw std::invalid_argument("The IP should be non-empty.");
  for (auto &c : cut) {
    if (c.empty())
      throw std::invalid_argument(
          "The IP shouldn't contain two consecutive dots or trailing dots.");
  }
  bool numeric = true;
  for (auto &c : cut) {
    for (auto &ch : c) numeric &= isdigit(ch);
  }
  if (cut.size() == 4u && numeric) {
    // Case: IPv4 address.
    // TODO: Check the correctness of the IPv4 address.
    for (auto &c : cut) {
      int v = 0;
      for (auto &ch : c) v = v * 10 + (ch - '0');
      if (v < 0 || v >= 256)
        throw std::invalid_argument("Invalid IPv4 address.");
    }
    try {
      in_addr_t addr = inet_addr(s.c_str());
      return addr;
    } catch (...) {
      throw;
    }
  }
  // Case: alias name
  hostent *host = gethostbyname(s.c_str());
  if (!host || !host->h_addr_list)
    throw std::invalid_argument("Invalid hostname.");
  in_addr **addr_list = reinterpret_cast<in_addr **>(host->h_addr_list);
  if (!addr_list || !addr_list[0])
    throw std::invalid_argument("Invalid hostname.");
  return addr_list[0]->s_addr;
}

std::pair<in_addr_t, uint16_t> ParseIPPort(const std::string &s) {
  size_t found = s.find(':');
  if (found == std::string::npos) Usage();
  try {
    in_addr_t ip = ParseIP(s.substr(0, found));
    uint16_t port = static_cast<uint16_t>(atoi(s.substr(found + 1).c_str()));
    return std::make_pair(ip, port);
  } catch (...) {
    throw;
  }
}

Argument ArgParse(int argc, const char **argv) {
  if (argc == 1) Usage();
  int i = 1;
  Argument args;
  while (i < argc) {
    if (argv[i][0] == '-') {
      if (i + 1 == argc) Usage();
      if (strcmp(argv[i], "-n") == 0)
        args.num_ping = static_cast<size_t>(atoi(argv[i + 1]));
      else if (strcmp(argv[i], "-t") == 0)
        args.timeout = static_cast<uint32_t>(atoi(argv[i + 1]));
      else if (strcmp(argv[i], "-i") == 0)
        args.ping_interval = static_cast<uint32_t>(atoi(argv[i + 1]));
      else
        throw std::invalid_argument("Unknown argument: " +
                                    std::string(argv[i]));
      i += 2;
    } else {
      std::string ip_port(argv[i]);
      args.push(ParseIPPort(ip_port));
      i += 1;
    }
  }
  return args;
}

// serialize the in_addr_t to human-readable IPv4 address
const char *Serialize(in_addr_t ip) {
  in_addr addr;
  addr.s_addr = ip;
  return inet_ntoa(addr);
}

void Error(in_addr_t ip, uint16_t port) {
  std::cout << "timeout when connecting to " << Serialize(ip) << ":" << port
            << std::endl;
}

void Success(in_addr_t ip, uint16_t port, std::chrono::microseconds rtt) {
  std::cout
      << "recv from " << Serialize(ip) << ":" << port << ", RTT = "
      << std::chrono::duration_cast<std::chrono::milliseconds>(rtt).count()
      << " msec" << std::endl;
}

std::chrono::microseconds ConnectTo(in_addr_t ip, uint16_t port,
                                    uint32_t timeout) {
  sockaddr_in svr;
  svr.sin_family = AF_INET;
  svr.sin_port = htons(port);
  svr.sin_addr.s_addr = ip;
  int svr_fd = 0;
  if ((svr_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    throw std::runtime_error("Failed at socket()");

  long args = fcntl(svr_fd, F_GETFL, NULL);
  if (args < 0) throw std::runtime_error("Failed at fcntl()");
  args |= O_NONBLOCK;
  if (fcntl(svr_fd, F_SETFL, args) <
      0)  // set the socket file descriptor to non-blocking mode.
    throw std::runtime_error("Failed at fcntl()");

  std::chrono::steady_clock::time_point start_time =
      std::chrono::steady_clock::now();

  int res = connect(svr_fd, reinterpret_cast<sockaddr *>(&svr), sizeof(svr));
  if (res < 0) {  // connection failed
    timeval tv;
    tv.tv_sec = timeout / 1000 * 2;
    tv.tv_usec = (timeout % 1000) * 2000;
    if (tv.tv_usec >= 1000000) {
      // select() crashes if the tv_usec is greater than 10^6
      tv.tv_usec -= 1000000;
      tv.tv_sec += 1;
    }
    fd_set fd;
    FD_ZERO(&fd);
    FD_SET(svr_fd, &fd);
    res = select(svr_fd + 1, nullptr, &fd, nullptr, &tv);
    if (res < 0 && errno != EINTR) {
      close(svr_fd);
      throw std::runtime_error("Failed at select()");
    }
    if (res == 0) {
      close(svr_fd);
      throw std::runtime_error("Connection timeout");
    }
  }
  std::chrono::steady_clock::time_point end_time =
      std::chrono::steady_clock::now();
  auto rtt = std::chrono::duration_cast<std::chrono::microseconds>(end_time -
                                                                   start_time);
  close(svr_fd);
  return rtt;
}

void PrintStat(const Statistic &stat, in_addr_t ip, uint16_t port) {
  std::cout << "--- " << Serialize(ip) << " ping statistics ---" << std::endl;
  std::cout << stat << std::endl;
}

static volatile bool terminated = false;

void SignalHandler(int sig) { terminated = true; }

}  // namespace

int main(int argc, const char **argv) {
  Argument args;
  try {
    args = ArgParse(argc, argv);
  } catch (std::invalid_argument &e) {
    std::cout << e.what() << std::endl;
    Usage();
  } catch (...) {
    std::cout << "Invalid hostname or IP." << std::endl;
    Usage();
  }
  signal(SIGINT, SignalHandler);  // handle SIGINT

  std::mutex mtx;  // to protect the standard output and the standard error

  auto TCPPing = [&mtx](in_addr_t ip, uint16_t port, size_t num_ping,
                        uint32_t timeout, uint32_t ping_interval) {
    Statistic stat;
    for (size_t p = 0; (num_ping == 0 ? true : p < num_ping) && !terminated;
         ++p) {
      try {
        auto rtt = ConnectTo(
            ip, port, timeout);  // round-trip time calculated in microseconds
        mtx.lock();
        if (rtt.count() > timeout * 1000) {
          Error(ip, port);
          stat.Failed();
        } else {
          Success(ip, port, rtt);
          double v = rtt.count() / 1000.0;
          stat.Success(v);
        }
        mtx.unlock();
      } catch (...) {
        mtx.lock();
        Error(ip, port);
        stat.Failed();
        mtx.unlock();
      }
      std::this_thread::sleep_for(std::chrono::milliseconds(
          ping_interval));  // sleep for `ping_interval` milliseconds between
                            // two consecutive pings.
    }
    return stat;
  };
  std::vector<std::future<Statistic>> fut(args.ip_ports.size());
  for (size_t i = 0; i < args.ip_ports.size(); ++i) {
    fut[i] = std::async(std::launch::async, TCPPing, args.ip_ports[i].first,
                        args.ip_ports[i].second, args.num_ping, args.timeout,
                        args.ping_interval);
  }
  for (size_t i = 0; i < args.ip_ports.size(); ++i) {
    auto statistic = fut[i].get();
    mtx.lock();
    PrintStat(statistic, args.ip_ports[i].first, args.ip_ports[i].second);
    mtx.unlock();
  }
  return 0;
}
